package com.hy.storage.mapper;


import com.hy.storage.domain.Storage;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface StorageMapper extends Mapper<Storage> {

    @Update("update tab_storage set total = total - #{currentUsed} , sold = sold + #{currentUsed} where product_id = #{productId}")
    int updateUsed(@Param("productId") long productId , @Param("currentUsed") long currentUsed);

    @Select("select * from tab_storage where product_id = #{productId}")
    Storage getStorageByPid(@Param("productId") long productId);

    @Update("update tab_storage set total = total + #{currentUsed} , sold = sold - #{currentUsed} where product_id = #{productId}")
    int storageCompensate(@Param("productId") long productId , @Param("currentUsed") long currentUsed);

}
