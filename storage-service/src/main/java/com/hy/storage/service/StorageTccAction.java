package com.hy.storage.service;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

@LocalTCC
public interface StorageTccAction {

    @TwoPhaseBusinessAction(name = "reduceStock",commitMethod = "commit",rollbackMethod = "rollback")
    public boolean reduceStock(BusinessActionContext businessActionContext, @BusinessActionContextParameter(paramName = "productId") long productId ,@BusinessActionContextParameter(paramName = "quantity") long quantity);


    public boolean commit(BusinessActionContext businessActionContext);

    public boolean rollback(BusinessActionContext businessActionContext);
}
