package com.hy.storage.service.impl;

import com.hy.storage.mapper.StorageMapper;
import com.hy.storage.service.StorageServiceXA;
import io.seata.core.context.RootContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
public class StorageServiceImplXA implements StorageServiceXA {
    @Resource
    StorageMapper storageMapper;

    @Override
    public boolean reduceStockByXA(long productId, long quantity) {
        String xid = RootContext.getXID();
        log.info("reduce  stock in transaction: " + xid);

        boolean res = storageMapper.updateUsed(productId, quantity) > 0 ? true : false;
        if (!res){
            throw new RuntimeException("Failed to call storage Service. ");
        }
        return res;
    }
}
