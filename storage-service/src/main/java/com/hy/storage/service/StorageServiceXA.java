package com.hy.storage.service;

public interface StorageServiceXA {

    public boolean reduceStockByXA(long productId, long quantity);
}
