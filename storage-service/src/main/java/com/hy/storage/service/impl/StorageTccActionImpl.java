package com.hy.storage.service.impl;

import com.hy.storage.domain.Storage;
import com.hy.storage.mapper.StorageMapper;
import com.hy.storage.service.StorageTccAction;
import com.hy.storage.util.IdempotentUtil;
import io.seata.rm.tcc.api.BusinessActionContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Objects;

@Slf4j
@Component
public class StorageTccActionImpl implements StorageTccAction {
    @Resource
    StorageMapper storageMapper;

    @Transactional
    @Override
    public boolean reduceStock(BusinessActionContext businessActionContext, long productId, long quantity) {
        if (Objects.nonNull(IdempotentUtil.getMarker(getClass(),businessActionContext.getXid()))){
            log.info("已执行过try阶段");
            return true;
        }
        Storage storage = storageMapper.getStorageByPid(productId);
        // 总数
        storage.setTotal(storage.getTotal() - quantity);

        // tcc 冻结shu
        storage.setFrozen(storage.getFrozen() + quantity);

        storageMapper.updateByPrimaryKeySelective(storage);
        log.info("减少库存：tcc一阶段 try 成功");
        IdempotentUtil.addMarker(getClass(),businessActionContext.getXid(),"marker");
        return true;
    }

    @Transactional
    @Override
    public boolean commit(BusinessActionContext businessActionContext) {
        if (Objects.isNull(IdempotentUtil.getMarker(getClass(),businessActionContext.getXid()))){
            log.info("已执行过commit阶段");
            return true;
        }
        long productId = Long.parseLong(businessActionContext.getActionContext("productId").toString());
        long quantity = Long.parseLong(businessActionContext.getActionContext("quantity").toString());

        Storage storage = storageMapper.getStorageByPid(productId);
        // 总数
        storage.setSold(storage.getSold() + quantity);
        // tcc 冻结shu
        storage.setFrozen(storage.getFrozen() - quantity);

        storageMapper.updateByPrimaryKeySelective(storage);
        log.info("减少库存：tcc二阶段 commit 成功");
        IdempotentUtil.removeMarker(getClass(),businessActionContext.getXid());
        return true;

    }

    @Transactional
    @Override
    public boolean rollback(BusinessActionContext businessActionContext) {
        if (Objects.isNull(IdempotentUtil.getMarker(getClass(),businessActionContext.getXid()))){
            log.info("已执行过rollback阶段");
            return true;
        }
        long productId = Long.parseLong(businessActionContext.getActionContext("productId").toString());
        long quantity = Long.parseLong(businessActionContext.getActionContext("quantity").toString());

        Storage storage = storageMapper.getStorageByPid(productId);
        // 总数
        storage.setTotal(storage.getTotal() + quantity);
        // tcc 冻结shu
        storage.setFrozen(storage.getFrozen() - quantity);

        storageMapper.updateByPrimaryKeySelective(storage);

        log.info("减少库存：tcc二阶段回滚成功");
        IdempotentUtil.removeMarker(getClass(),businessActionContext.getXid());
        return true;

    }
}
