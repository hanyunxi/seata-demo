package com.hy.storage.service;

import io.seata.rm.tcc.api.BusinessActionContext;
import org.apache.ibatis.annotations.Param;

public interface StorageService {

    String updateUsed(@Param("productId") long productId , @Param("currentUsed") long currentUsed);

    public boolean reduceStock(long productId, long quantity);

    public boolean reduceStockByXA(long productId, long quantity);
}
