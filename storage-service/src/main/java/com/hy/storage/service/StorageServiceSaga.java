package com.hy.storage.service;

/**
 * seata saga 模式  补偿机制
 */
public interface StorageServiceSaga {

    /**
     *  库存扣减
     * @param businessKey
     * @param storageException
     * @param productId
     * @param quantity
     * @return
     */
    public boolean reduceStockBySaga(String businessKey,String storageException,long productId, long quantity);

    /**
     * 库存补偿
     * @param businessKey
     * @param productId
     * @param quantity
     * @return
     */
    boolean storageCompensate(String businessKey,long productId, long quantity);

}
