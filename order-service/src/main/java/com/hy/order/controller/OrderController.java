package com.hy.order.controller;

import cn.hutool.core.util.IdUtil;
import com.hy.order.domain.Order;
import com.hy.order.service.OrderService;
import com.hy.order.service.OrderServiceSaga;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class OrderController {
    @Autowired
    OrderService orderService;
    @Autowired
    OrderServiceSaga orderServiceSaga;

    @RequestMapping("/say")
    public String say(String businessKey){
        return "say";
    }
    @RequestMapping("/nosay")
    public String nosay(String businessKey){
        return "nosay";
    }

    @GetMapping("/order/create")
    public String create(long userId , long productId){
        Order order = new Order();
        order.setId(IdUtil.createSnowflake(1,1).nextId())
        .setCount(1)
                .setMoney(BigDecimal.valueOf(88))
                .setProductId(productId)
                .setUserId(userId)
                .setStatus(0);
        return orderService.create(order);
    }
    // http://localhost:8500/order/tcc/create?userId=1&productId=1
    @GetMapping("/order/tcc/create")
    public String tccCreate(long userId , long productId){
        Order order = new Order();
        order.setId(IdUtil.createSnowflake(1,1).nextId())
                .setCount(1)
                .setMoney(BigDecimal.valueOf(88))
                .setProductId(productId)
                .setUserId(userId)
                .setStatus(0);
        return orderService.createTccOrder(order);
    }

    // http://localhost:8500/order/tcc/create?userId=1&productId=1
    @GetMapping("/order/xa/create")
    public String xa(long userId , long productId){
        Order order = new Order();
        order.setId(IdUtil.createSnowflake(1,1).nextId())
                .setCount(1)
                .setMoney(BigDecimal.valueOf(88))
                .setProductId(productId)
                .setUserId(userId)
                .setStatus(0);
        return orderService.createXAOrder(order);
    }

    @GetMapping("/order/saga/create")
    public String createSagaOrder(String businessKey,long userId , long productId){
        Order order = new Order();
        order.setId(IdUtil.createSnowflake(1,1).nextId())
                .setCount(1)
                .setMoney(BigDecimal.valueOf(88))
                .setProductId(productId)
                .setUserId(userId)
                .setStatus(0);
        return orderServiceSaga.createSagaOrder(businessKey,order) ? "success" : "false";
    }

    @GetMapping("/order/saga/orderCompensate")
    public String orderCompensate(String businessKey,long userId , long productId){
        Order order = new Order();
        order.setId(IdUtil.createSnowflake(1,1).nextId())
                .setCount(1)
                .setMoney(BigDecimal.valueOf(88))
                .setProductId(productId)
                .setUserId(userId)
                .setStatus(0);
        return orderServiceSaga.orderCompensate(businessKey,order) ? "success" : "false";
    }






}
