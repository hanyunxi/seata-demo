package com.hy.order.service.impl;

import com.hy.order.domain.Order;
import com.hy.order.mapper.OrderMapper;
import com.hy.order.service.OrderServiceXA;
import io.seata.core.context.RootContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class OrderServiceImplXA implements OrderServiceXA {
    @Autowired
    OrderMapper orderMapper;

    @Override
    public boolean createXAOrder(Order order) {
        String xid = RootContext.getXID();

        log.info("create order xid: "+xid);
        boolean res = orderMapper.insertSelective(order) > 0 ? true : false;
        if (!res){
            throw new RuntimeException("Failed to call order Service. ");
        }
        return res;
    }
}
