package com.hy.order.service;

import com.hy.order.domain.Order;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

@LocalTCC
public interface OrderTccAction {

    @TwoPhaseBusinessAction(name = "createOrder",commitMethod = "commit",rollbackMethod = "rollback")
    public boolean createOrder(BusinessActionContext businessActionContext, @BusinessActionContextParameter(paramName = "order") Order order);


    public boolean commit(BusinessActionContext businessActionContext);

    public boolean rollback(BusinessActionContext businessActionContext);
}
