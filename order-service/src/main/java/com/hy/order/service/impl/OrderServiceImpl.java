package com.hy.order.service.impl;

import com.hy.order.domain.Order;
import com.hy.order.mapper.OrderMapper;
import com.hy.order.service.OrderService;
import com.hy.order.service.OrderServiceSaga;
import com.hy.order.service.OrderServiceXA;
import com.hy.order.service.OrderTccAction;
import com.hy.order.util.ApplicationContextUtils;
import io.seata.saga.engine.StateMachineEngine;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class OrderServiceImpl implements OrderService {

    @Resource
    OrderMapper orderMapper;
    @Autowired
    OrderTccAction orderTccAction;
    @Autowired
    OrderServiceXA orderServiceXA;
    @Autowired
    OrderServiceSaga orderServiceSaga;


    @Override
    public String create(Order order) {
        int i =1/0;
        return orderMapper.insertSelective(order) > 0 ? "true" : "false";
    }


    @Override
    //@GlobalTransactional
    @Transactional
    public String createTccOrder(Order order) {
        boolean res = orderTccAction.createOrder(null, order);
        // 异常测试
        //int i = 1/0;
        if (!res){
            throw  new RuntimeException("创建订单异常！");
        }
        return "success";
    }

    @Override
    public String createXAOrder(Order order) {
        boolean res = orderServiceXA.createXAOrder(order);
        // 异常测试
        //int i = 1/0;
        if (!res){
            throw  new RuntimeException("创建订单异常！");
        }
        return "success";
    }

    /**
     *  saga
     * @param order
     * @return
     */
    @Override
    public String createSagaOrder(Order order) {

        StateMachineEngine stateMachineEngine = (StateMachineEngine) ApplicationContextUtils.getApplicationContext().getBean("stateMachineEngine");







        return null;
    }
}
