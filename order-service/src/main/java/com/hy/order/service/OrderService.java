package com.hy.order.service;

import com.hy.order.domain.Order;

public interface OrderService {
    String create(Order order);

    public String createTccOrder(Order order);

    public String createXAOrder(Order order);

    public String createSagaOrder(Order order);
}
