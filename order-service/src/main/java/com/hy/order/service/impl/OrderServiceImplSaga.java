package com.hy.order.service.impl;

import cn.hutool.core.lang.Snowflake;
import com.alibaba.fastjson.JSON;
import com.hy.order.domain.Order;
import com.hy.order.mapper.OrderMapper;
import com.hy.order.service.OrderServiceSaga;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
public class OrderServiceImplSaga implements OrderServiceSaga {

    @Resource
    OrderMapper orderMapper;

    @Override
    public boolean createSagaOrder(String businessKey, Order order) {
        log.info("创建订单开始, businessKey：{}, order: {}", businessKey, JSON.toJSONString(order));
        if (order.getId() == null){
            return false;
        }
        order.setStatus(1);
        int res = orderMapper.insertSelective(order);

        log.info("创建订单结束, businessKey：{}, result: {}", businessKey, res);
        return true;
    }

    @Override
    public boolean orderCompensate(String businessKey, Order order) {
        Long id = order.getId();
        log.info("订单补偿开始，业务id：{},orderNo={}", businessKey,id );
        if (id == null) {
            return false;
        }
        int res = orderMapper.deleteByPrimaryKey(order);
        log.info("订单补偿结束，业务id：{},orderNo={},结果:{}", businessKey, id, res);
        return true;
    }
}
