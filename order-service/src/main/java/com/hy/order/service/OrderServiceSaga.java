package com.hy.order.service;

import com.hy.order.domain.Order;

public interface OrderServiceSaga {

    /**
     * 创建订单
     * @param businessKey
     * @param order
     * @return
     */
    public boolean createSagaOrder(String businessKey,Order order);

    /**
     * 订单补偿
     * @param businessKey
     * @param order
     * @return
     */
    public boolean orderCompensate(String businessKey,Order order);

}
