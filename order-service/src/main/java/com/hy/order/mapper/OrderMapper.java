package com.hy.order.mapper;

import com.hy.order.domain.Order;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
@Repository
public interface OrderMapper extends Mapper<Order> {


}
