package com.hy.businesss.domain;


import java.math.BigDecimal;

/**
 * @author jianjun.ren
 * @since 2021/02/16
 */

public class Order {


    private Long id;

    private Long userId;

    private Long productId;

    private int count;

    private BigDecimal money;

    private int status;
}

