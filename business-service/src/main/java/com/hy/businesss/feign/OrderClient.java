package com.hy.businesss.feign;

import com.hy.businesss.domain.Order;
import com.hy.businesss.fallback.OrderFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@FeignClient(value = "order-service"
       // ,fallback = OrderFallback.class
)
public interface OrderClient {

    @RequestMapping("/say")
    public String say(@RequestParam("businessKey") String businessKey);

    @RequestMapping("/nosay")
    public String nosay(@RequestParam("businessKey") String businessKey);

    @GetMapping("/order/create")
    String create(@RequestParam("userId") long userId , @RequestParam("productId") long productId);

    @GetMapping("/order/tcc/create")
    public String createTccOrder(@RequestParam("userId") long userId , @RequestParam("productId") long productId);

    @GetMapping("/order/xa/create")
    public String createXAOrder(@RequestParam("userId") long userId , @RequestParam("productId") long productId);


    @GetMapping("/order/saga/create")
    public String createSagaOrder(@RequestParam("businessKey") String businessKey,@RequestParam("userId") long userId , @RequestParam("productId") long productId);

    @GetMapping("/order/saga/orderCompensate")
    public String orderCompensate(@RequestParam("businessKey") String businessKey,@RequestParam("userId") long userId , @RequestParam("productId") long productId);
}


