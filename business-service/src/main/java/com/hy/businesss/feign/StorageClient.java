package com.hy.businesss.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@FeignClient(value = "storage-service")
public interface StorageClient {

    @GetMapping("storage/change")
    String changeStorage(@RequestParam("productId") long productId ,@RequestParam("used")  int used);

    @GetMapping("storage/tcc/change")
    public boolean reduceStock(@RequestParam("productId") long productId ,@RequestParam("sold") long sold);

    @GetMapping("storage/xa/change")
    public boolean changeXaStorage(@RequestParam("productId") long productId ,@RequestParam("sold") long sold);

    @RequestMapping(value = "/storageDeduct")
    boolean storageDeduct(@RequestParam("businessKey") String businessKey,
                          @RequestParam("productId") Integer productId,
                          @RequestParam("sold") Integer sold,
                          @RequestParam("storageException") String storageException);

    @RequestMapping(value = "/storageCompensate")
    boolean storageCompensate(@RequestParam("businessKey") String businessKey,
                              @RequestParam("productId") Integer productId,
                              @RequestParam("sold") Integer sold);
}
