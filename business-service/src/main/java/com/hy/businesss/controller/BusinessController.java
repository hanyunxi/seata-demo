package com.hy.businesss.controller;

import com.hy.businesss.domain.Result;
import com.hy.businesss.service.BusinessServiceAT;
import com.hy.businesss.service.BusinessServiceSaga;
import com.hy.businesss.service.BusinessServiceTCC;
import com.hy.businesss.service.BusinessServiceXA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BusinessController {

    @Autowired
    BusinessServiceAT businessServiceAT;
    @Autowired
    BusinessServiceTCC businessServiceTCC;
    @Autowired
    BusinessServiceXA businessServiceXA;
    @Autowired
    BusinessServiceSaga businessServiceSaga;


    @RequestMapping("/buy")
    public Result buy(long userId , long productId){
        businessServiceAT.operateByAT(userId, productId);
        return new Result(HttpStatus.OK.value(), "ok","查询成功！");
    }

    @RequestMapping("/tcc/buy")
    public Result tccBuy(long userId , long productId){
        businessServiceTCC.operateByTCC(userId, productId);
        return new Result(HttpStatus.OK.value(), "ok","查询成功！");
    }
    // http://localhost:8900/xa/buy?userId=1&productId=1
    @RequestMapping("/xa/buy")
    public Result xaBuy(long userId , long productId){
        businessServiceXA.operateByXA(userId, productId);
        return new Result(HttpStatus.OK.value(), "ok","查询成功！");
    }

    /**
     * http://127.0.0.1:8900/saga/buy?userId=1&productId=1&storageException=
     * @param userId
     * @param productId
     * @return
     */
    @RequestMapping("/saga/buy")
    public Result sagaBuy(long userId , long productId){
        businessServiceSaga.operateBySaga(userId, productId);
        return new Result(HttpStatus.OK.value(), "ok","查询成功！");
    }
    @RequestMapping("/saga/say")
    public Result sagaSay(){
        businessServiceSaga.get();
        return new Result(HttpStatus.OK.value(), "ok","查询成功！");
    }





}
