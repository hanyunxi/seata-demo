package com.hy.businesss.service.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.hy.businesss.domain.Result;
import com.hy.businesss.service.BusinessServiceAT;
import com.hy.businesss.feign.OrderClient;
import com.hy.businesss.feign.StorageClient;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class BusinessServiceImplAT implements BusinessServiceAT {

    @Autowired
    StorageClient storageClient;

    @Autowired
    OrderClient orderClient;

    int num = 0;

    @GlobalTransactional
    @SentinelResource(value = "buy",
            blockHandler = "blockHandler",
            fallback = "fallback")
    public Result operateByAT(long userId, long productId) {
        orderClient.create(userId, productId);
        storageClient.changeStorage(userId,1);
        return new Result(HttpStatus.OK.value(), "ok","查询成功！");
    }

    //Throwable时进入的方法
    public Result fallback(Throwable throwable) {
        return new Result(HttpStatus.OK.value(), num,"接口发生异常： "+throwable.getMessage());
    }
    //Throwable时进入的方法
    public Result fallback( long userId ,  long productId,Throwable throwable) {
        return new Result(HttpStatus.OK.value(), num,"接口发生异常： "+throwable.getMessage());
    }
}
