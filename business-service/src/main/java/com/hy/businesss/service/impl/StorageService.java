package com.hy.businesss.service.impl;

import com.hy.businesss.feign.StorageClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

@Service
public class StorageService {
    @Resource
    StorageClient storageClient;



    boolean storageDeduct(@RequestParam("businessKey") String businessKey,
                          @RequestParam("productId") Integer productId,
                          @RequestParam("sold") Integer sold
            //,@RequestParam("storageException") String storageException
    ){
        //return storageClient.storageDeduct(businessKey, productId, sold, storageException);
        return storageClient.storageDeduct(businessKey, productId, sold, "");
    }

    boolean storageCompensate(@RequestParam("businessKey") String businessKey,
                              @RequestParam("productId") Integer productId,
                              @RequestParam("sold") Integer sold){
       return storageClient.storageCompensate(businessKey, productId, sold);
    }

}
