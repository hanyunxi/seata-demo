package com.hy.businesss.service;

import com.hy.businesss.domain.Result;

public interface BusinessServiceSaga {


    public Result operateBySaga(long userId, long productId);

    public String get();
}
