package com.hy.businesss.service.impl;

import ch.qos.logback.classic.net.LoggingEventPreSerializationTransformer;
import com.hy.businesss.domain.Result;
import com.hy.businesss.feign.OrderClient;
import com.hy.businesss.feign.StorageClient;
import com.hy.businesss.service.BusinessServiceSaga;
import com.hy.businesss.util.ApplicationContextUtils;
import com.sun.activation.registries.LogSupport;
import io.seata.saga.engine.StateMachineEngine;
import io.seata.saga.statelang.domain.ExecutionStatus;
import io.seata.saga.statelang.domain.StateMachineInstance;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class BusinessServiceImplSaga implements BusinessServiceSaga {
    @Autowired
    StorageClient storageClient;
    @Autowired
    OrderClient orderClient;
    @Autowired
    ApplicationContext applicationContext;





    public Result operateBySaga(long userId, long productId
            //,String storageException
    ) {
        StateMachineEngine stateMachineEngine = (StateMachineEngine) applicationContext.getBean("stateMachineEngine");

        Map<String, Object> startParams = new HashMap<String, Object>(16);
        String businessKey = String.valueOf(System.currentTimeMillis());

        startParams.put("businessKey", businessKey);
        startParams.put("userId", userId);
        startParams.put("productId", productId);
        //startParams.put("storageException", storageException);
        startParams.put("sold", 1);

        //sync test
        StateMachineInstance inst = stateMachineEngine.startWithBusinessKey("sagaBuy", null, businessKey, startParams);
        if (ExecutionStatus.SU.equals(inst.getStatus())) {
            log.info("saga transaction commit succeed. XID: {}", inst.getId());
            inst = stateMachineEngine.getStateMachineConfig().getStateLogStore().getStateMachineInstanceByBusinessKey(businessKey, null);
            if (ExecutionStatus.SU.equals(inst.getStatus())) {
                return new Result(HttpStatus.OK.value(), "ok","查询成功！");
            }
            log.info("saga transaction execute failed. XID:{},status:{}", inst.getId(), inst.getStatus());
            return new Result(HttpStatus.OK.value(), "fail","查询成功！");
        }
        return new Result(HttpStatus.OK.value(), "fail","查询成功！");
    }

    public String get() {

        StateMachineEngine stateMachineEngine = (StateMachineEngine) applicationContext.getBean("stateMachineEngine");

        Map<String, Object> startParams = new HashMap<String, Object>(16);
        String businessKey = String.valueOf(System.currentTimeMillis());

        startParams.put("businessKey", businessKey);

        //sync test
        StateMachineInstance inst = stateMachineEngine.startWithBusinessKey("get", null, businessKey, startParams);
        if (ExecutionStatus.SU.equals(inst.getStatus())) {
            log.info("saga transaction commit succeed. XID: {}", inst.getId());
            inst = stateMachineEngine.getStateMachineConfig().getStateLogStore().getStateMachineInstanceByBusinessKey(businessKey, null);
            if (ExecutionStatus.SU.equals(inst.getStatus())) {
                return "ok";
            }
            log.info("saga transaction execute failed. XID:{},status:{}", inst.getId(), inst.getStatus());
            return "fail";
        }
        return "fail";
    }
}
