package com.hy.businesss.service.impl;

import com.hy.businesss.feign.OrderClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

@Service
public class OrderService {
    @Resource
    OrderClient orderClient;

    @RequestMapping("/say")
    public String say(@RequestParam("businessKey") String businessKey){
        return orderClient.say(businessKey);
    }

    @RequestMapping("/nosay")
    public String nosay(@RequestParam("businessKey") String businessKey){
     return orderClient.nosay(businessKey);
    }


    @GetMapping("/order/saga/create")
    public String createSagaOrder(@RequestParam("businessKey") String businessKey,@RequestParam("userId") long userId , @RequestParam("productId") long productId){
        return orderClient.createSagaOrder(businessKey, userId, productId);
    }

    @GetMapping("/order/saga/orderCompensate")
    public String orderCompensate(@RequestParam("businessKey") String businessKey,@RequestParam("userId") long userId , @RequestParam("productId") long productId){
        return orderClient.orderCompensate(businessKey, userId, productId);
    }



}
