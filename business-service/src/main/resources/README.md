# seata-saga

#### 介绍
Seata 是一款开源的分布式事务解决方案，致力于提供高性能和简单易用的分布式事务服务。Seata 将为用户提供了 AT、TCC、SAGA 和 XA 事务模式，为用户打造一站式的分布式解决方案。

#### 安装教程

1.  新建数据库seata_server\seata_account\seata_order\seata_stock，分别到对应新建的数据库中执行seata-saga/sql目录下的文件seata_server.sql\seata_account.sql\seata_order.sql\seata_stock.sql

2.  解压seata-saga/software/seata-server-1.4.2.zip，运行seata服务器，sh bin/seata-server.sh、双击bin目录下的seata-server.bat

2.1  修改conf/registry.conf和conf/file.conf 配置

设置type、设置serverAddr为你的nacos节点地址。

**注意这里有一个坑，serverAddr不能带‘http://’前缀**

~~~text
registry {
  # file 、nacos 、eureka、redis、zk、consul、etcd3、sofa
  type = "nacos"

  nacos {
    serverAddr = "127.0.0.1"
    namespace = ""
    cluster = "default"
  }
}
config {
  # file、nacos 、apollo、zk、consul、etcd3
  type = "nacos"
  nacos {
    serverAddr = "127.0.0.1"
    namespace = ""
    cluster = "default"
  }
}

~~~

```text
store {
  ## store mode: file、db、redis
  mode = "db"
  ## rsa decryption public key
  publicKey = ""
  ## file store property
  file {
    ## store location dir
    dir = "sessionStore"
    # branch session size , if exceeded first try compress lockkey, still exceeded throws exceptions
    maxBranchSessionSize = 16384
    # globe session size , if exceeded throws exceptions
    maxGlobalSessionSize = 512
    # file buffer size , if exceeded allocate new buffer
    fileWriteBufferCacheSize = 16384
    # when recover batch read size
    sessionReloadReadSize = 100
    # async, sync
    flushDiskMode = async
  }

  ## database store property
  db {
    ## the implement of javax.sql.DataSource, such as DruidDataSource(druid)/BasicDataSource(dbcp)/HikariDataSource(hikari) etc.
    datasource = "druid"
    ## mysql/oracle/postgresql/h2/oceanbase etc.
    dbType = "postgresql"
    driverClassName = "org.postgresql.Driver"
    ## if using mysql to store the data, recommend add rewriteBatchedStatements=true in jdbc connection param
    url = "jdbc:postgresql://192.168.1.154:5432/seata_test"
    user = "postgres"
    password = "Cnhis.com@2021"
    minConn = 5
    maxConn = 100
    globalTable = "global_table"
    branchTable = "branch_table"
    lockTable = "lock_table"
    queryLimit = 100
    maxWait = 5000
  }
}

```

3.  下载nacos-server-2.0.4（https://github.com/alibaba/nacos/releases）

解压nacos-server-2.0.4.zip，运行nacos服务器，sh bin/startup.sh、bin/startup.cmd -m standalone

导入nacos_config配置，seata-saga/software/nacos_config_export_xxx

#### 使用说明

1.  提交订单接口

http://127.0.0.1:8900/saga/buy??userId=1&productId=1&storageException=

其中accountException=true表示在account服务发生异常，storageException=true表示在storage服务器发生异常，为空表示正常提交订单。

##### Saga 状态机配置说明
https://help.aliyun.com/document_detail/172550.html

##### seata脚本
https://github.com/seata/seata/tree/develop/script

