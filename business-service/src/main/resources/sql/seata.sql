/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80017
 Source Host           : localhost:3306
 Source Schema         : seata

 Target Server Type    : MySQL
 Target Server Version : 80017
 File Encoding         : 65001

 Date: 02/05/2022 15:43:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for branch_table
-- ----------------------------
DROP TABLE IF EXISTS `branch_table`;
CREATE TABLE `branch_table`  (
  `branch_id` bigint(128) NOT NULL,
  `xid` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `transaction_id` bigint(20) NULL DEFAULT NULL,
  `resource_group_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `resource_id` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `branch_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT NULL,
  `client_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `application_data` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gmt_create` datetime(6) NULL DEFAULT NULL,
  `gmt_modified` datetime(6) NULL DEFAULT NULL,
  PRIMARY KEY (`branch_id`) USING BTREE,
  INDEX `idx_xid`(`xid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for global_table
-- ----------------------------
DROP TABLE IF EXISTS `global_table`;
CREATE TABLE `global_table`  (
  `xid` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `transaction_id` bigint(20) NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `application_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `transaction_service_group` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `transaction_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `timeout` int(11) NULL DEFAULT NULL,
  `begin_time` bigint(20) NULL DEFAULT NULL,
  `application_data` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gmt_create` datetime(0) NULL DEFAULT NULL,
  `gmt_modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`xid`) USING BTREE,
  INDEX `idx_gmt_modified_status`(`gmt_modified`, `status`) USING BTREE,
  INDEX `idx_transaction_id`(`transaction_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for lock_table
-- ----------------------------
DROP TABLE IF EXISTS `lock_table`;
CREATE TABLE `lock_table`  (
  `row_key` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `xid` varchar(96) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `transaction_id` bigint(20) NULL DEFAULT NULL,
  `branch_id` bigint(20) NOT NULL,
  `resource_id` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `table_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pk` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gmt_create` datetime(0) NULL DEFAULT NULL,
  `gmt_modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`row_key`) USING BTREE,
  INDEX `idx_branch_id`(`branch_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for seata_state_inst
-- ----------------------------
DROP TABLE IF EXISTS `seata_state_inst`;
CREATE TABLE `seata_state_inst`  (
  `id` varchar(48) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'id',
  `machine_inst_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'state machine instance id',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'state name',
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'state type',
  `service_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'service name',
  `service_method` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'method name',
  `service_type` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'service type',
  `business_key` varchar(48) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'business key',
  `state_id_compensated_for` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'state compensated for',
  `state_id_retried_for` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'state retried for',
  `gmt_started` datetime(3) NOT NULL COMMENT 'start time',
  `is_for_update` tinyint(1) NULL DEFAULT NULL COMMENT 'is service for update',
  `input_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT 'input parameters',
  `output_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT 'output parameters',
  `status` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'status(SU succeed|FA failed|UN unknown|SK skipped|RU running)',
  `excep` blob NULL COMMENT 'exception',
  `gmt_updated` datetime(3) NULL DEFAULT NULL COMMENT 'update time',
  `gmt_end` datetime(3) NULL DEFAULT NULL COMMENT 'end time',
  PRIMARY KEY (`id`, `machine_inst_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for seata_state_machine_def
-- ----------------------------
DROP TABLE IF EXISTS `seata_state_machine_def`;
CREATE TABLE `seata_state_machine_def`  (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'id',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'name',
  `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'tenant id',
  `app_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'application name',
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'state language type',
  `comment_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'comment',
  `ver` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'version',
  `gmt_create` datetime(3) NOT NULL COMMENT 'create time',
  `status` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'status(AC:active|IN:inactive)',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT 'content',
  `recover_strategy` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'transaction recover strategy(compensate|retry)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of seata_state_machine_def
-- ----------------------------
INSERT INTO `seata_state_machine_def` VALUES ('e2fb4202173a2e9e132a60b0b57612ea', 'sagaBuy', '000001', 'SEATA', 'STATE_LANG', 'create order then reduce storage in a transaction', '0.0.1', '2022-05-02 00:13:25.578', 'AC', '{\r\n  \"nodes\": [\r\n    {\r\n      \"type\": \"node\",\r\n      \"size\": \"110*48\",\r\n      \"shape\": \"flow-rect\",\r\n      \"color\": \"#1890FF\",\r\n      \"label\": \"OrderClient\",\r\n      \"stateId\": \"OrderClient\",\r\n      \"stateType\": \"ServiceTask\",\r\n      \"stateProps\": {\r\n        \"ServiceName\": \"OrderClient\",\r\n        \"ServiceMethod\": \"createSagaOrder\",\r\n        \"Next\": \"ChoiceState\",\r\n        \"Input\": [\r\n          \"$.[businessKey]\",\r\n          \"$.[userId]\",\r\n          \"$.[productId]\"\r\n        ],\r\n        \"Output\": {\r\n          \"orderResult\": \"$.#root\"\r\n        },\r\n        \"Status\": {\r\n          \"#root == \'success\'\": \"SU\",\r\n          \"#root == \'false\'\": \"FA\",\r\n          \"$Exception{java.lang.Throwable}\": \"UN\"\r\n        },\r\n        \"Retry\": []\r\n      },\r\n      \"x\": 439.875,\r\n      \"y\": 137.5,\r\n      \"id\": \"bc7cab6f\"\r\n    },\r\n    {\r\n      \"type\": \"node\",\r\n      \"size\": \"110*48\",\r\n      \"shape\": \"flow-capsule\",\r\n      \"color\": \"#722ED1\",\r\n      \"label\": \"orderCompensate\",\r\n      \"stateId\": \"orderCompensate\",\r\n      \"stateType\": \"Compensation\",\r\n      \"stateProps\": {\r\n        \"ServiceName\": \"orderClient\",\r\n        \"ServiceMethod\": \"orderCompensate\",\r\n        \"Input\": [\r\n          \"$.[businessKey]\",\r\n          \"$.[userId]\",\r\n          \"$.[productId]\"\r\n        ],\r\n        \"Output\": {\r\n          \"orderResult\": \"$.#root\"\r\n        },\r\n        \"Status\": {\r\n          \"#root == \'success\'\": \"SU\",\r\n          \"#root == \'false\'\": \"FA\",\r\n          \"$Exception{java.lang.Throwable}\": \"UN\"\r\n        },\r\n        \"Retry\": []\r\n      },\r\n      \"x\": 220.875,\r\n      \"y\": 130,\r\n      \"id\": \"71abfd50\"\r\n    },\r\n    {\r\n      \"type\": \"node\",\r\n      \"size\": \"80*72\",\r\n      \"shape\": \"flow-rhombus\",\r\n      \"color\": \"#13C2C2\",\r\n      \"label\": \"Choice\",\r\n      \"stateId\": \"Choice1\",\r\n      \"stateType\": \"Choice\",\r\n      \"x\": 438.375,\r\n      \"y\": 253,\r\n      \"id\": \"4f1db1e1\",\r\n      \"stateProps\": {\r\n        \"Type\": \"Choice\",\r\n        \"Choices\": [\r\n          {\r\n            \"Expression\": \"[orderResult] == \'\'success\",\r\n            \"Next\": \"reduceStock\"\r\n          }\r\n        ],\r\n        \"Default\": \"fail\"\r\n      }\r\n    },\r\n    {\r\n      \"type\": \"node\",\r\n      \"size\": \"110*48\",\r\n      \"shape\": \"flow-rect\",\r\n      \"color\": \"#1890FF\",\r\n      \"label\": \"reduceStock\",\r\n      \"stateId\": \"reduceStock\",\r\n      \"stateType\": \"ServiceTask\",\r\n      \"stateProps\": {\r\n        \"ServiceName\": \"storageClient\",\r\n        \"ServiceMethod\": \"storageDeduct\",\r\n        \"Input\": [\r\n          \"$.[businessKey]\",\r\n          \"$.[productId]\",\r\n          \"$.[sold]\"\r\n        ],\r\n        \"Output\": {\r\n          \"storageResult\": \"$.#root\"\r\n        },\r\n        \"Status\": {\r\n          \"#root == true\": \"SU\",\r\n          \"#root == false\": \"FA\",\r\n          \"$Exception{java.lang.Throwable}\": \"UN\"\r\n        },\r\n        \"Retry\": []\r\n      },\r\n      \"x\": 437.375,\r\n      \"y\": 360,\r\n      \"id\": \"f339456c\"\r\n    },\r\n    {\r\n      \"type\": \"node\",\r\n      \"size\": \"72*72\",\r\n      \"shape\": \"flow-circle\",\r\n      \"color\": \"#05A465\",\r\n      \"label\": \"Succeed\",\r\n      \"stateId\": \"Succeed1\",\r\n      \"stateType\": \"Succeed\",\r\n      \"x\": 438.375,\r\n      \"y\": 485,\r\n      \"id\": \"75f82ec4\"\r\n    },\r\n    {\r\n      \"type\": \"node\",\r\n      \"size\": \"110*48\",\r\n      \"shape\": \"flow-capsule\",\r\n      \"color\": \"#722ED1\",\r\n      \"label\": \"storageCompensate\",\r\n      \"stateId\": \"storageCompensate\",\r\n      \"stateType\": \"Compensation\",\r\n      \"stateProps\": {\r\n        \"ServiceName\": \"storageClient\",\r\n        \"ServiceMethod\": \"storageCompensate\",\r\n        \"Input\": [\r\n          \"$.[businessKey]\",\r\n          \"$.[productId]\",\r\n          \"$.[sold]\"\r\n        ],\r\n        \"Output\": {\r\n          \"storageResult\": \"$.#root\"\r\n        },\r\n        \"Status\": {\r\n          \"#root == true\": \"SU\",\r\n          \"#root == false\": \"FA\",\r\n          \"$Exception{java.lang.Throwable}\": \"UN\"\r\n        },\r\n        \"Retry\": []\r\n      },\r\n      \"x\": 211.875,\r\n      \"y\": 361,\r\n      \"id\": \"91b41cf7\"\r\n    },\r\n    {\r\n      \"type\": \"node\",\r\n      \"size\": \"110*48\",\r\n      \"shape\": \"flow-capsule\",\r\n      \"color\": \"red\",\r\n      \"label\": \"Fail\",\r\n      \"stateId\": \"Fail\",\r\n      \"stateType\": \"CompensationTrigger\",\r\n      \"x\": 722.375,\r\n      \"y\": 282,\r\n      \"id\": \"25abdafc\",\r\n      \"stateProps\": {\r\n        \"Type\": \"Fail\",\r\n        \"ErrorCode\": \"PURCHASE_FAILED\",\r\n        \"Message\": \"purchase failed\"\r\n      }\r\n    },\r\n    {\r\n      \"type\": \"node\",\r\n      \"size\": \"72*72\",\r\n      \"shape\": \"flow-circle\",\r\n      \"color\": \"#FA8C16\",\r\n      \"label\": \"Start\",\r\n      \"stateId\": \"Start2\",\r\n      \"stateType\": \"Start\",\r\n      \"stateProps\": {\r\n        \"StateMachine\": {\r\n          \"Name\": \"sagaBuy\",\r\n          \"Comment\": \"create order then reduce storage in a transaction\",\r\n          \"Version\": \"0.0.1\"\r\n        },\r\n        \"Next\": \"OrderClient\"\r\n      },\r\n      \"x\": 437.375,\r\n      \"y\": 30,\r\n      \"id\": \"d41f067f\"\r\n    },\r\n    {\r\n      \"type\": \"node\",\r\n      \"size\": \"72*72\",\r\n      \"shape\": \"flow-circle\",\r\n      \"color\": \"red\",\r\n      \"label\": \"Fail\",\r\n      \"stateId\": \"Fail2\",\r\n      \"stateType\": \"Fail\",\r\n      \"stateProps\": {\r\n        \"Type\": \"Fail\",\r\n        \"ErrorCode\": \"PURCHASE_FAILED\",\r\n        \"Message\": \"purchase failed\"\r\n      },\r\n      \"x\": 720.375,\r\n      \"y\": 151,\r\n      \"id\": \"55605a5f\"\r\n    }\r\n  ],\r\n  \"edges\": [\r\n    {\r\n      \"source\": \"4f1db1e1\",\r\n      \"sourceAnchor\": 2,\r\n      \"target\": \"f339456c\",\r\n      \"targetAnchor\": 0,\r\n      \"id\": \"51115820\",\r\n      \"stateProps\": {\r\n        \"Expression\": \"\",\r\n        \"Default\": false\r\n      }\r\n    },\r\n    {\r\n      \"source\": \"bc7cab6f\",\r\n      \"sourceAnchor\": 3,\r\n      \"target\": \"71abfd50\",\r\n      \"targetAnchor\": 1,\r\n      \"id\": \"4676a7fb\",\r\n      \"style\": {\r\n        \"lineDash\": \"4\"\r\n      }\r\n    },\r\n    {\r\n      \"source\": \"bc7cab6f\",\r\n      \"sourceAnchor\": 2,\r\n      \"target\": \"4f1db1e1\",\r\n      \"targetAnchor\": 0,\r\n      \"id\": \"997e7276\"\r\n    },\r\n    {\r\n      \"source\": \"f339456c\",\r\n      \"sourceAnchor\": 2,\r\n      \"target\": \"75f82ec4\",\r\n      \"targetAnchor\": 0,\r\n      \"id\": \"66127b79\"\r\n    },\r\n    {\r\n      \"source\": \"f339456c\",\r\n      \"sourceAnchor\": 3,\r\n      \"target\": \"91b41cf7\",\r\n      \"targetAnchor\": 1,\r\n      \"id\": \"30cf81b0\",\r\n      \"style\": {\r\n        \"lineDash\": \"4\"\r\n      }\r\n    },\r\n    {\r\n      \"source\": \"d41f067f\",\r\n      \"sourceAnchor\": 2,\r\n      \"target\": \"bc7cab6f\",\r\n      \"targetAnchor\": 0,\r\n      \"id\": \"a84c9d95\"\r\n    },\r\n    {\r\n      \"source\": \"bc7cab6f\",\r\n      \"sourceAnchor\": 1,\r\n      \"target\": \"55605a5f\",\r\n      \"targetAnchor\": 3,\r\n      \"id\": \"98aa747a\"\r\n    },\r\n    {\r\n      \"source\": \"25abdafc\",\r\n      \"sourceAnchor\": 0,\r\n      \"target\": \"55605a5f\",\r\n      \"targetAnchor\": 2,\r\n      \"id\": \"f6360c7c\"\r\n    }\r\n  ]\r\n}', NULL);

-- ----------------------------
-- Table structure for seata_state_machine_inst
-- ----------------------------
DROP TABLE IF EXISTS `seata_state_machine_inst`;
CREATE TABLE `seata_state_machine_inst`  (
  `id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'id',
  `machine_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'state machine definition id',
  `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'tenant id',
  `parent_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'parent id',
  `gmt_started` datetime(3) NOT NULL COMMENT 'start time',
  `business_key` varchar(48) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'business key',
  `start_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT 'start parameters',
  `gmt_end` datetime(3) NULL DEFAULT NULL COMMENT 'end time',
  `excep` blob NULL COMMENT 'exception',
  `end_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT 'end parameters',
  `status` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'status(SU succeed|FA failed|UN unknown|SK skipped|RU running)',
  `compensation_status` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'compensation status(SU succeed|FA failed|UN unknown|SK skipped|RU running)',
  `is_running` tinyint(1) NULL DEFAULT NULL COMMENT 'is running(0 no|1 yes)',
  `gmt_updated` datetime(3) NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unikey_buz_tenant`(`business_key`, `tenant_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of seata_state_machine_inst
-- ----------------------------
INSERT INTO `seata_state_machine_inst` VALUES ('192.168.1.4:8091:7422190169449603608', '6daa7f91eb124cbd11203c6ac36963b0', '000001', NULL, '2022-05-01 23:40:32.352', '1651419632352', '{\"@type\":\"java.util.HashMap\",\"storageException\":\"\",\"sold\":1,\"productId\":1L,\"_business_key_\":\"1651419632352\",\"businessKey\":\"1651419632352\",\"userId\":1L}', '2022-05-01 23:40:32.511', NULL, '{\"@type\":\"java.util.HashMap\",\"storageException\":\"\",\"sold\":1,\"_statemachine_error_code_\":\"PURCHASE_FAILED\",\"orderResult\":\"success\",\"productId\":1L,\"_business_key_\":\"1651419632352\",\"businessKey\":\"1651419632352\",\"_statemachine_error_message_\":\"purchase failed\",\"userId\":1L}', 'UN', NULL, 0, '2022-05-01 23:40:32.512');
INSERT INTO `seata_state_machine_inst` VALUES ('192.168.1.4:8091:7422190169449603722', '6daa7f91eb124cbd11203c6ac36963b0', '000001', NULL, '2022-05-01 23:42:14.451', '1651419734451', '{\"@type\":\"java.util.HashMap\",\"storageException\":\"\",\"sold\":1,\"productId\":1L,\"_business_key_\":\"1651419734451\",\"businessKey\":\"1651419734451\",\"userId\":1L}', '2022-05-01 23:42:14.614', NULL, '{\"@type\":\"java.util.HashMap\",\"storageException\":\"\",\"sold\":1,\"_statemachine_error_code_\":\"PURCHASE_FAILED\",\"orderResult\":\"success\",\"productId\":1L,\"_business_key_\":\"1651419734451\",\"businessKey\":\"1651419734451\",\"_statemachine_error_message_\":\"purchase failed\",\"userId\":1L}', 'UN', NULL, 0, '2022-05-01 23:42:14.615');
INSERT INTO `seata_state_machine_inst` VALUES ('192.168.1.4:8091:7422190169449604350', '6daa7f91eb124cbd11203c6ac36963b0', '000001', NULL, '2022-05-01 23:47:22.592', '1651420042592', '{\"@type\":\"java.util.HashMap\",\"storageException\":\"\",\"sold\":1,\"productId\":1L,\"_business_key_\":\"1651420042592\",\"businessKey\":\"1651420042592\",\"userId\":1L}', '2022-05-01 23:47:22.712', NULL, '{\"@type\":\"java.util.HashMap\",\"storageException\":\"\",\"sold\":1,\"_statemachine_error_code_\":\"PURCHASE_FAILED\",\"orderResult\":\"success\",\"productId\":1L,\"_business_key_\":\"1651420042592\",\"businessKey\":\"1651420042592\",\"_statemachine_error_message_\":\"purchase failed\",\"userId\":1L}', 'UN', NULL, 0, '2022-05-01 23:47:22.713');

-- ----------------------------
-- Table structure for undo_log
-- ----------------------------
DROP TABLE IF EXISTS `undo_log`;
CREATE TABLE `undo_log`  (
  `branch_id` bigint(128) NOT NULL COMMENT 'branch transaction id',
  `xid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'global transaction id',
  `context` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'undo_log context,such as serialization',
  `rollback_info` longblob NOT NULL COMMENT 'rollback info',
  `log_status` int(11) NOT NULL COMMENT '0:normal status,1:defense status',
  `log_created` datetime(6) NOT NULL COMMENT 'create datetime',
  `log_modified` datetime(6) NOT NULL COMMENT 'modify datetime',
  UNIQUE INDEX `ux_undo_log`(`xid`, `branch_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'AT transaction mode undo table' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
